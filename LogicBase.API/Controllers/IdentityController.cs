using System.Collections.Generic;
using System.Threading.Tasks;
using Logicbase.Models.DataModels.User;
using LogicBase.API.Core.Business.BusinessLogic;
using LogicBase.API.Core.Business.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Logicbase.Models.ViewModels.Account;
using System.Linq;
using Newtonsoft.Json;
using Logicbase.Models.DataModels.Security;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;

namespace LogicBase.API.Controllers
{
    [Route("api/[controller]/[action]")]
    
    public class IdentityController : Controller
    {
        private readonly UserManager<LogicUser> _userManager;
        private readonly SignInManager<LogicUser> _signInManager;
        private readonly IMessageService _messageService;
        private readonly ILogger _logger;
        private readonly JsonSerializerSettings _serializerSettings;   
        private readonly JwtIssuerOptions _jwtOptions;     
        public IdentityController(IOptions<JwtIssuerOptions> jwtOptions,
                                    UserManager<LogicUser> userManager,
                                    ILoggerFactory loggerFactory,
                                    SignInManager<LogicUser> signInManager,
                                    IMessageService messageService)
        {
             _jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_jwtOptions);
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._messageService = messageService;
            _logger = loggerFactory.CreateLogger<IdentityController>();
             _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(ApplicationUser applicationUser)
        {
            var identity = await GetIdentity(applicationUser.UserName, applicationUser.Password);
            if (identity == null)
            {
                _logger.LogInformation($"Invalid username ({applicationUser.UserName}) or password ({applicationUser.Password})");
                return BadRequest("Invalid credentials");
            }

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, applicationUser.UserName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, 
                                ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), 
                                ClaimValueTypes.Integer64));

            var user = _userManager.FindByIdAsync(identity.Claims.FirstOrDefault(k=> k.Type == ClaimTypes.NameIdentifier).Value);
            var roles = _userManager.GetRolesAsync(user.Result);
            foreach(var role in roles.Result){
                claims.Add(new Claim(ClaimTypes.Role, role, Issuer));
            }

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            
            // Serialize and return the response
            var response = new
            {
                access_token = encodedJwt,
                expires_in = (int)_jwtOptions.ValidFor.TotalSeconds,
                // admin = identity.IsAdministrator(),
                // fullname = identity.FullName(),
                username = identity.Name
            };


            Response.ContentType = "application/json";
            var json = JsonConvert.SerializeObject(response, _serializerSettings);
            return new OkObjectResult(json);
        }
 
       private async Task<ClaimsIdentity> GetIdentity(string email, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(email);
                var claims = await _userManager.GetClaimsAsync(user);
                var identity = new ClaimsIdentity(new GenericIdentity(email, "Token"), claims);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));

                return identity;
            }
        
            // Credentials are invalid, or account doesn't exist
            return null;
        }

 

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(string email, string password, string repassword, string verificationUrl)
        {
            if (password != repassword)
            {
                return BadRequest(ErrorManager.GetError(101));
            }

            var newUser = new LogicUser()
            {
                UserName = email,
                Email = email
            };

            var userCreationResult = await _userManager.CreateAsync(newUser, password);
            if (!userCreationResult.Succeeded)
            {
                var moreDetails = userCreationResult.Errors.Aggregate("", (current, error) => current + (error.Description + "\n"));
                return BadRequest(ErrorManager.GetError(102, moreDetails));
            }

            var emailConfirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(newUser);
            var tokenVerificationUrl = verificationUrl + "?userId=" + newUser.Id + "&token=" + emailConfirmationToken ;
            await _messageService.Send(email, "Verify your email",
                $"Click <a href=\"{tokenVerificationUrl}\">here</a> to verify your email");

            return Ok();
           
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return BadRequest(ErrorManager.GetError(100));
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return BadRequest(ErrorManager.GetError(103));
            }
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(ErrorManager.GetError(103));
            }
                
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return BadRequest(ErrorManager.GetError(100));
                }

               var code = await _userManager.GeneratePasswordResetTokenAsync(user);
              // var callbackUrl = Url.Action(nameof(ResetPassword), "Accounts", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                var callbackUrl = Url.RouteUrl(model.ResetPasswordUrl, new {userId = user.Id, code = code},
                    protocol: HttpContext.Request.Scheme);
                await _messageService.Send(model.Email, "Reset Password", $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");// _emailSender.SendEmailAsync(model.Email, "Reset Password", $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return Ok();
            }

            // If we got this far, something failed, redisplay form
            return BadRequest();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return BadRequest();
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest();
        }
        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
        if (options == null) throw new ArgumentNullException(nameof(options));

        if (options.ValidFor <= TimeSpan.Zero)
        {
            throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
        }

        if (options.SigningCredentials == null)
        {
            throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
        }

        if (options.JtiGenerator == null)
        {
            throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
        }
      }

    /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
    private static long ToUnixEpochDate(DateTime date)
      => (long)Math.Round((date.ToUniversalTime() - 
                           new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                          .TotalSeconds);
    private static string Issuer => "http://localhost:5000/";

    //FOR  TEST   PURPOSES
    // YOU CAN DELETE THEM
        [HttpGet]
        [Authorize]    
        public  IEnumerable<string> Test()
        {
            
              return new string[] { "value4", "value2" };
        }
    
        [HttpGet]
        [Authorize(Roles="User")]   //  [Authorize(Policy="UserPolicy")]    // it works because of startupcs policy definition
        public  IEnumerable<string> TestUser()
        {
         
              return new string[] { "user", "value2" };
        }    

        [HttpGet]
        [Authorize(Roles="Admin")]    
        public  IEnumerable<string> TestAdmin()
        {
         
              return new string[] { "Admin", "value2" };
        }   
    }
    
}
