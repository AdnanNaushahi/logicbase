using LogicBase.API.Core.Business.BusinessLogic;
using LogicBase.API.Core.Business.Interfaces;
using LogicBase.API.Core.Data.Interfaces;
using LogicBase.API.Core.Data.Repositories;
using LogicBase.API.Core.Data.UnitOfWorks;

using Microsoft.Extensions.DependencyInjection;


namespace LogicBase.API
{
    public static class DependencyInjections
    {
        public static void Injections(ref IServiceCollection services){
          services.AddTransient<IMessageService, MessageService>();
          services.AddTransient<ISmtpConfigRepository, SmtpConfigRepository>();
          services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}