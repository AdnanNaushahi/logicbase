﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LogicBase.API.Migrations
{
    public partial class Roles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentAdd', 'ContentAdd')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentUpdate', 'ContentUpdate')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentDelete', 'ContentDelete')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentPull', 'ContentPull')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentPublish', 'ContentPublish')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentPublish', 'ContentPublish')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'ContentApprove', 'ContentApprove')");

            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'AccountAdmin', 'AccountAdmin')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'UserAdmin', 'UserAdmin')");
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id, Name, NormalizedName) VALUES ((SELECT UUID()), 'FormAdmin', 'FormAdmin')");

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentAdd'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentUpdate'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentDelete'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentPull'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentPublish'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentPublish'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='ContentApprove'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='AccountAdmin'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='UserAdmin'");
            migrationBuilder.Sql("DELETE FROM aspnetroles WHERE Name='FormAdmin'");
        }
    }
}
