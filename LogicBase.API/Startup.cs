using System;
using LogicBase.API.Core.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MySQL.Data.Entity.Extensions;
using Logicbase.Models.DataModels.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NSwag.AspNetCore;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Logicbase.Models.DataModels.Security;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using Logicbase.Models.ViewModels.Generics;

namespace LogicBase.API
{
    public class Startup
    {

        private const string SecretKey = "needtogetthisfromenvironment";
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();
            services.AddDbContext<LogicDbContext>(options =>
                 options.UseMySQL(Configuration.GetConnectionString("DefaultConnection"),
                    optionsBuilder => optionsBuilder.MigrationsAssembly("LogicBase.API")));

            services.AddIdentity<LogicUser, IdentityRole>(options =>
                {
                    options.Cookies.ApplicationCookie.AutomaticChallenge = false;   // important: to prevent redirection to login page
                })
                .AddEntityFrameworkStores<LogicDbContext>()
                .AddDefaultTokenProviders();

           PasswordParameters(ref services);

           DependencyInjections.Injections(ref services);

            services.Configure<AppSettings>(Configuration.GetSection("ApplicationSettings"));

            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
                
            });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                .RequireAuthenticatedUser()
                                .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });    

            services.AddAuthorization(options =>
                {
                    options.AddPolicy("AdminPolicy", policy => policy.RequireRole("Admin", "User"));
                    options.AddPolicy("UserPolicy", policy => policy.RequireRole("User"));                    
                });    
            services.AddMvcCore()
                .AddAuthorization() // Note - this is on the IMvcBuilder, not the service collection
                .AddJsonFormatters(options => options.ContractResolver = new CamelCasePropertyNamesContractResolver());                                
        }
         

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            BearerToken(ref app);
            app.UseIdentity();
            app.UseSwaggerUi(typeof(Startup).GetTypeInfo().Assembly, new SwaggerUiOwinSettings());
            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default",
                                template: "api/{controller=Default}/{action=Get}/{id?}");
            });
        }

         public void BearerToken(ref IApplicationBuilder app){
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = false,
                ValidIssuer = "ExampleIssuer",
                // Validate the JWT Audience (aud) claim
                ValidateAudience = false,
                ValidAudience = "ExampleAudience",
                // Validate the token expiry
                ValidateLifetime = true,
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };
            
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });
        }

        public void PasswordParameters(ref IServiceCollection services)
        {
             services.Configure<IdentityOptions>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;

                options.SignIn.RequireConfirmedEmail = true;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);

                // User settings
                options.User.RequireUniqueEmail = true;
            });
        }

    }
}
