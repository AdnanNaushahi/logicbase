
using LogicBase.API.Core.Data.Interfaces;
using LogicBase.API.Core.Data.Repositories;
using System;

namespace LogicBase.API.Core.Data.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LogicDbContext _context;

        public UnitOfWork(LogicDbContext context)
        {
            _context = context;
            SmtpConfigs = new SmtpConfigRepository(_context);

        } 
     
        public ISmtpConfigRepository SmtpConfigs{get; private set;}
        public IAccountRepository Accounts{get; private set;}


        public void Complete()
        {
             _context.SaveChanges();
        }

       public void Dispose()
        {
            _context.Dispose();
        }

    }
}
