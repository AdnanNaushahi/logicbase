using Logicbase.Models.DataModels.Email;
using LogicBase.API.Core.Data.Interfaces;

namespace LogicBase.API.Core.Data.Repositories
{
    public class SmtpConfigRepository : Repository<SmtpConfig>, ISmtpConfigRepository 
    {
        public SmtpConfigRepository(LogicDbContext context) : base(context)
        {
        }
 
        public LogicDbContext LogicDbContext
        {
            get{return Context as LogicDbContext;}
        }  

    }
}