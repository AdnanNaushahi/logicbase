using LogicBase.API.Core.Data.Interfaces;
using Logicbase.Models.DataModels.Account;
using System.Collections.Generic;
using System.Linq;

namespace LogicBase.API.Core.Data.Repositories
{
    public class AccountRepository : Repository<Account>, IAccountRepository 
    {
        public AccountRepository(LogicDbContext context) : base(context)
        {
            
        }  

        public IEnumerable<AccountType> GetAccountTypes(int pageIndex, int pageSize = 10)
        {
          return LogicDbContext.AccountTypes
               // .Include(c => c.Author)
                .OrderBy(c => c.AccountTypeName)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public LogicDbContext LogicDbContext
        {
            get{return Context as LogicDbContext;}
        }  

    }
}

 