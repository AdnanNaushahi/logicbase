using Logicbase.Models.DataModels.User;
using LogicBase.API.Core.Data.Interfaces;


namespace LogicBase.API.Core.Data.Repositories
{
    public class UserProfilesRepository : Repository<UserProfile>, IUserProfilesRepository
    {
        public UserProfilesRepository(LogicDbContext context) : base(context)
        {
        }


    }
}
