using Logicbase.Models.DataModels.Error;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Logicbase.Models.DataModels.User;
using Logicbase.Models.DataModels.Email;
using Logicbase.Models.DataModels.Account;
using Logicbase.Models.DataModels.Generics;

namespace LogicBase.API.Core.Data
{
    public class LogicDbContext :IdentityDbContext
    {
         public LogicDbContext(DbContextOptions<LogicDbContext> options) 
           : base(options)
       {}

        public DbSet<Error> Errors { get; set; }
        public DbSet<SmtpConfig> SmtpConfigs {get; set;}
        public DbSet<LogicUser> LogicUsers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountHistory> AccountHistories { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<AccountUserCross> AccountUserCrosses { get; set; }

        public DbSet<PhoneNumberType> PhoneNumberTypes { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityUser>()
                .HasDiscriminator<int>("Type")
                .HasValue<IdentityUser>(0)
                .HasValue<LogicUser>(1);
        }

    }
}