using Logicbase.Models.DataModels.Account;
using System.Collections.Generic;
using System;


namespace LogicBase.API.Core.Data.Interfaces
{
    public interface IAccountRepository: IRepository<Account>
    {
          IEnumerable<AccountType> GetAccountTypes(int pageIndex, int pageSize = 10);
    }
}