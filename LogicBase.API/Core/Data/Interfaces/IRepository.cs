using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace LogicBase.API.Core.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        // void Update(TEntity model);

        // Task Delete(int id);

        // Task<IEnumerable<TEntity>> GetAll(int? pageIndex = null,
        //     Expression<Func<TEntity, bool>> filter = null,
        //     Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        //     string includeProperties = "");

        

        // int PageSize { get; set; }

        // void Include<TProperty>(Expression<Func<TEntity, TProperty>> navigationPropertyPath);
    }
}
