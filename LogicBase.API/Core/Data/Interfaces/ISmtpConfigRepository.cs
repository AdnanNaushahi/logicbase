using Logicbase.Models.DataModels.Email;

namespace LogicBase.API.Core.Data.Interfaces
{
    public interface ISmtpConfigRepository: IRepository<SmtpConfig>
    {
         
    }
}