using Logicbase.Models.DataModels.User;
namespace LogicBase.API.Core.Data.Interfaces
{

    public interface IUserProfilesRepository : IRepository<UserProfile>
    {
        // Person GetUserMenuWithMenuItems(int menuId);
    }
}
