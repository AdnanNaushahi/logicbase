
using System;

namespace LogicBase.API.Core.Data.Interfaces
{

    public interface IUnitOfWork : IDisposable
    {
      void Complete();
       ISmtpConfigRepository SmtpConfigs {get;}
       IAccountRepository Accounts{get; }
    }
}
