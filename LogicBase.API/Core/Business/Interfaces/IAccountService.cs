using Logicbase.Models.DataModels.Account;
using System.Collections.Generic;
using System;


namespace LogicBase.API.Core.Business.Interfaces
{
    public interface IAccountService
    {
         Guid NewAccount(Account account);
    }
}