using System.Collections.Generic;
using System.Threading.Tasks;
using Logicbase.Models.DataModels.Email;
using Microsoft.AspNetCore.Server.Kestrel.Internal.Http;
using MimeKit;

namespace LogicBase.API.Core.Business.Interfaces
{
    public interface IMessageService
    {
        Task Send(string email, string subject, string message);    
        Task<bool> SendEmailAsync(List<MailboxAddress> toAddresses, Logicbase.Models.ViewModels.Email.MessageBody Mail,  SmtpConfig smtpConfig, bool writeAsText = false);

    }
}