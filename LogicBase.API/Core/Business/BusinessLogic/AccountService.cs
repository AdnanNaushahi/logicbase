using Logicbase.Models.DataModels.Account;
using LogicBase.API.Core.Data.Interfaces;
using System.Collections.Generic;
using Logicbase.Models.ViewModels.Generics;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;


namespace LogicBase.API.Core.Business.BusinessLogic
{
    public class AccountService
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public AccountService( ILoggerFactory loggerFactory, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _logger = loggerFactory.CreateLogger<MessageService>();
        }


        public Guid NewAccount(Account account)
        {
            if(account.Id == null)  
            { 
                account.Id = Guid.NewGuid() ; 
            }
            _unitOfWork.Accounts.Add(account);
            _unitOfWork.Complete();
            return account.Id;
        }


    }
}