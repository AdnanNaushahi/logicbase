using System.Collections.Generic;
using System.Linq;
using Logicbase.Models.DataModels.Error;

namespace LogicBase.API.Core.Business.BusinessLogic
{
//This class may require refactoring. it may be placed in database or some kind of constant may be. 
    // TODO:Check if refactoring is needed
    public static class ErrorManager
    {
        private static List<Error> _errorList;

        /// <summary>
        /// Provide error id to get error object. if you want to give more details use moreDetails parameter. (It's optional)
        /// </summary>
        /// <param name="errorId">Error Id. It is a predefined id number for error.</param>
        /// <param name="moreDetails">Optional paramter to provide more details to client.</param>
        /// <returns></returns>
        public static Error GetError(int errorId, string moreDetails = null)
        {
            if (_errorList == null || _errorList.Count < 1)
            {
                Init();
            }
            var retVal = _errorList.FirstOrDefault(k => k.ErrorCode == errorId);
            if (moreDetails != null)
            {
                retVal.MoreDetails = moreDetails;
            }
            return retVal;
        }

        private static void Init()
        {
            _errorList = new List<Error>
        {
            new Error(100, "Missing or Wrong parameters"),
            new Error(101, "Passwords don't match"),
            new Error(102, "Error creating user"),
            new Error(103, "Error verifying user"),
            new Error(104, "User account locked out."),
            new Error(105, "Invalid login attempt."),
            new Error(106, "Invalid login attempt."),
            new Error(107, "Error in two factor authentication.")
        };
        }
    }
}