using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Logicbase.Models.DataModels.Email;
using Logicbase.Models.DataModels.Generics;
using Logicbase.Models.ViewModels.Email;
using LogicBase.API.Core.Business.Interfaces;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Server.Kestrel.Internal.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using LogicBase.API.Core.Data;
using LogicBase.API.Core.Data.Interfaces;
using Logicbase.Models.ViewModels.Generics;


namespace LogicBase.API.Core.Business.BusinessLogic
{
    public class MessageService : IMessageService
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _appSettings;

        public MessageService( ILoggerFactory loggerFactory, IUnitOfWork unitOfWork, IOptions<AppSettings> appSettings)
        {
            _unitOfWork = unitOfWork;
            _logger = loggerFactory.CreateLogger<MessageService>();
            _appSettings = appSettings.Value;
        }

        Task IMessageService.Send(string email, string subject, string message)
        {
            var emailMessage = $"To: {email}\nSubject: {subject}\nMessage: {message}\n\n";
            var toAddresses = new List<MailboxAddress>();
            toAddresses.Add(new MailboxAddress(email, email));

            var Mail = new Logicbase.Models.ViewModels.Email.MessageBody();
            Mail.Subject = subject;
            Mail.TextBody = message;

            SmtpConfig smtpConfig = new SmtpConfig();
            return SendEmailAsync(toAddresses, Mail, smtpConfig, false);
        }

        public SmtpConfig GetSmtp()
        {
              return  _unitOfWork.SmtpConfigs.Get(new Guid(_appSettings.DefaultSMTPGuid));
        }

        public async Task<bool> SendEmailAsync(List<MailboxAddress> toAddresses, Logicbase.Models.ViewModels.Email.MessageBody Mail,  SmtpConfig smtpConfig, bool writeAsText = false)
        {
            var message = PrepareEmailAsync(new MailboxAddress(smtpConfig.FromName, smtpConfig.FromAddress), toAddresses,Mail);
            if(writeAsText)
            {
                var emailMessage = ConvertEmailToStringForLogging(toAddresses, Mail);
                WriteEmailLogToFile(emailMessage, smtpConfig.localTextPath);
            }
            WriteEmailLogToDb(toAddresses, Mail, smtpConfig);
            return await SendAsync(message, smtpConfig);
        }

        private void WriteEmailLogToFile(string emailMessage, string localTextPath)
        {
            File.AppendAllText($"{localTextPath}\\emails{DateTime.Now.ToString("yyyyMMdd")}.txt", emailMessage);
        }
        private void WriteEmailLogToDb(List<MailboxAddress> toAddresses, Logicbase.Models.ViewModels.Email.MessageBody Mail,  SmtpConfig smtpConfig)
        {
            //Do Nothing for now
        }

        private string ConvertEmailToStringForLogging(List<MailboxAddress> toAddresses, Logicbase.Models.ViewModels.Email.MessageBody Mail)
        {
            var emailMessage = "";
            if(Mail.ReplyToEmail.Length > 0)
            {
                emailMessage += $"From: {Mail.ReplyToName} - {Mail.ReplyToEmail} \n";
            }
            emailMessage += $"To: {string.Join(", ", toAddresses)}\nSubject: {Mail.Subject}\nMessage: {Mail.TextBody}\n\n\n";
            return emailMessage;
        }
        private MimeMessage PrepareEmailAsync(MailboxAddress fromAddress, List<MailboxAddress> toAddresses, Logicbase.Models.ViewModels.Email.MessageBody Mail)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(fromAddress);
            emailMessage.To.AddRange(toAddresses);
            emailMessage.Subject = Mail.Subject;
         
            var builder = new BodyBuilder ();
            builder.TextBody = Mail.TextBody;
            if(Mail.HtmlBody.Length > 0)
            {
                builder.HtmlBody = string.Format (Mail.HtmlBody);
            }

            foreach(var attachment in Mail.Attachments)
            {
                if(!attachment.IsEmbedded)
                {
                    builder.Attachments.Add(attachment.FilePath);
                }
            }
            emailMessage.Body = builder.ToMessageBody();
            return  emailMessage;
        }

        private async Task<bool> SendAsync(MimeMessage emailMessage, SmtpConfig smtpConfig)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.LocalDomain = smtpConfig.Server;                
                    await client.ConnectAsync(smtpConfig.Server, smtpConfig.Port, SecureSocketOptions.Auto);
                    await client.AuthenticateAsync(smtpConfig.User, smtpConfig.Pass);
                    await client.SendAsync(emailMessage).ConfigureAwait(false);
                    await client.DisconnectAsync(true).ConfigureAwait(false);
                    return true;
                }
                catch (Exception e)
                {
                    _logger.LogInformation(e.InnerException.ToString());
                    return false;
                }

            }
        }

    }
}