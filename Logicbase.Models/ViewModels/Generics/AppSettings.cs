namespace Logicbase.Models.ViewModels.Generics
{
    public class AppSettings
    {
        public string DefaultSMTPGuid { get; set; }
    }
}