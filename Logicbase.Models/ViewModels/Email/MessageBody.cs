using System.Collections.Generic;
 
namespace Logicbase.Models.ViewModels.Email
{
    public class MessageBody
    {
       public string ReplyToName{get; set;}
       public string ReplyToEmail{get; set;}
       public string Subject {get; set;}
       public string TextBody {get; set;} 
       public string HtmlBody  {get; set;} 
       public List<Attachment> Attachments {get; set;} 
    }
}