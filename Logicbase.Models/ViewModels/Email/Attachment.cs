namespace Logicbase.Models.ViewModels.Email
{
    public class Attachment
    {
        public string FilePath { get; set; }
        public bool IsEmbedded {get; set;}
        
    }
}