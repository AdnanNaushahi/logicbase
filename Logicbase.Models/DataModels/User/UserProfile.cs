using System;
using System.Collections.Generic;
using Logicbase.Models.DataModels.Generics;

namespace Logicbase.Models.DataModels.User
{
    public class UserProfile
    {
        public Guid Id { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
    }
}