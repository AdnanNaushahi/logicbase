using System;

namespace Logicbase.Models.DataModels.Account
{
    public class AccountType
    {
       public Guid Id { get; set; }
       public  string AccountTypeName { get; set; }        
       public  int MaxUser { get; set; }        
       public  int MaxSite { get; set; }        
    }
}