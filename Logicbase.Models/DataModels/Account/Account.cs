using System;

namespace Logicbase.Models.DataModels.Account
{
    public class Account
    {
        public  Guid Id { get; set; }    
        public string AccountName {get; set;}
        
    }
}