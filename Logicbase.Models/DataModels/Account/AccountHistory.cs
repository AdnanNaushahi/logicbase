using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Logicbase.Models.DataModels.Account
{
    public class AccountHistory
    {
          public  Guid Id { get; set; }    
          public Guid AccountId { get; set; }
          public DateTime FromDate { get; set; }
          public DateTime? ToDate { get; set; }
          public int MaxUser { get; set; }
          public int MaxSite  { get; set; }
          public Guid AccountTypeId { get; set; }

          [ForeignKey("AccountTypeId")] 
          public AccountType AccountType {get; set;}

          [ForeignKey("AccountId")] 
          public Account Account {get; set;}
    }
}