using System;
using System.ComponentModel.DataAnnotations.Schema;
using Logicbase.Models.DataModels.User;

namespace Logicbase.Models.DataModels.Account
{
    public class AccountUserCross
    {
          public  Guid Id { get; set; }    
          public Guid AccountId { get; set; }
          public string UserId { get; set; }
          public bool PrimaryUser { get; set; }

          [ForeignKey("AccountId")] 
          public Account Account {get; set;}

          [ForeignKey("UserId")] 
          public LogicUser LogicUser {get; set;}          

    }
}