using System;

namespace Logicbase.Models.DataModels.Generics
{
    public class PhoneNumberType
    {
        public Guid Id { get; set; }
        public string PhoneType { get; set; }
    }
}