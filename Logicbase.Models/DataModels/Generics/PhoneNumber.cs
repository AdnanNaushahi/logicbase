using System;

namespace Logicbase.Models.DataModels.Generics
{
    public class PhoneNumber
    {
        public Guid Id { get; set; }
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string Number { get; set; }
        public PhoneNumberType PhoneNumberType { get; set; }

    }
}