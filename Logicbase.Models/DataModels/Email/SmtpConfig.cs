using System;

namespace Logicbase.Models.DataModels.Email
{
    public class SmtpConfig
    {
        public Guid Id { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string Server { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public int Port { get; set; }
        public string localTextPath {get; set;}
    }
}