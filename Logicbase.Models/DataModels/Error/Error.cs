using System;

namespace Logicbase.Models.DataModels.Error
{
    public class Error
    {
        public Error(int errorCode,   string errorDetails)
        {
            ErrorCode = errorCode;
            ErrorDetails = errorDetails;
        }
        public Guid Id { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorDetails { get; set; }
        public string MoreDetails { get; set; }
    }
}