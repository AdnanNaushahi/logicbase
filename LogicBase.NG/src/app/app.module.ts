import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { HomeSectionComponent } from './home-section/home-section.component';
import { AboutSectionComponent } from './about-section/about-section.component';
import { WhyUsSectionComponent } from './why-us-section/why-us-section.component';
import { HowWeWorkSectionComponent } from './how-we-work-section/how-we-work-section.component';
import { NumberCounterSectionComponent } from './number-counter-section/number-counter-section.component';
import { WhatWeOfferSectionComponent } from './what-we-offer-section/what-we-offer-section.component';
import { OurBestFeaturesSectionComponent } from './our-best-features-section/our-best-features-section.component';
import { ContactSectionComponent } from './contact-section/contact-section.component';
import { CopyrightSectionComponent } from './copyright-section/copyright-section.component';
import { BackToTopButtonSectionComponent } from './back-to-top-button-section/back-to-top-button-section.component';
import { PopupsSectionComponent } from './popups-section/popups-section.component';
import { ButtonsComponent } from './buttons/buttons.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavigationComponent,
    HomeSectionComponent,
    AboutSectionComponent,
    WhyUsSectionComponent,
    HowWeWorkSectionComponent,
    NumberCounterSectionComponent,
    WhatWeOfferSectionComponent,
    OurBestFeaturesSectionComponent,
    ContactSectionComponent,
    CopyrightSectionComponent,
    BackToTopButtonSectionComponent,
    PopupsSectionComponent,
    ButtonsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
