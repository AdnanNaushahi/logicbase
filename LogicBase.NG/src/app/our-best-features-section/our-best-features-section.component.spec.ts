import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurBestFeaturesSectionComponent } from './our-best-features-section.component';

describe('OurBestFeaturesSectionComponent', () => {
  let component: OurBestFeaturesSectionComponent;
  let fixture: ComponentFixture<OurBestFeaturesSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurBestFeaturesSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurBestFeaturesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
