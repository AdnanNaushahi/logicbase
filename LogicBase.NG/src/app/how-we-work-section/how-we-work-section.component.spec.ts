import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowWeWorkSectionComponent } from './how-we-work-section.component';

describe('HowWeWorkSectionComponent', () => {
  let component: HowWeWorkSectionComponent;
  let fixture: ComponentFixture<HowWeWorkSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowWeWorkSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowWeWorkSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
