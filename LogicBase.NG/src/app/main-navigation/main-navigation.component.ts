import { Component, OnInit } from '@angular/core';
import { ButtonsComponent } from '../buttons/buttons.component' ;

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {
  menuItem1 = 'HomePage' ;
  constructor() { }

  ngOnInit() {
  }

}
