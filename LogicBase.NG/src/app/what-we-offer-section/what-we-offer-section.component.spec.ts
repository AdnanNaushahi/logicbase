import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatWeOfferSectionComponent } from './what-we-offer-section.component';

describe('WhatWeOfferSectionComponent', () => {
  let component: WhatWeOfferSectionComponent;
  let fixture: ComponentFixture<WhatWeOfferSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatWeOfferSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWeOfferSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
