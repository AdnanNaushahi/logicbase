import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberCounterSectionComponent } from './number-counter-section.component';

describe('NumberCounterSectionComponent', () => {
  let component: NumberCounterSectionComponent;
  let fixture: ComponentFixture<NumberCounterSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberCounterSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberCounterSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
