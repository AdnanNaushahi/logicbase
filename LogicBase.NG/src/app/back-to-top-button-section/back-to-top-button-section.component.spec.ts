import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackToTopButtonSectionComponent } from './back-to-top-button-section.component';

describe('BackToTopButtonSectionComponent', () => {
  let component: BackToTopButtonSectionComponent;
  let fixture: ComponentFixture<BackToTopButtonSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackToTopButtonSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackToTopButtonSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
