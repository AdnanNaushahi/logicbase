import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-back-to-top-button-section',
  templateUrl: './back-to-top-button-section.component.html',
  styleUrls: ['./back-to-top-button-section.component.css']
})
export class BackToTopButtonSectionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
