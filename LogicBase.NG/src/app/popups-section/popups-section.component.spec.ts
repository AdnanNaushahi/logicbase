import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsSectionComponent } from './popups-section.component';

describe('PopupsSectionComponent', () => {
  let component: PopupsSectionComponent;
  let fixture: ComponentFixture<PopupsSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
