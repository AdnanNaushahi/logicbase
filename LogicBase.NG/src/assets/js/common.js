/* ==============================================
 Website Preloader
 =============================================== */

$(window).on("load",function() {
    'use strict';
    // Animate loader off screen
    $(".se-pre-con").fadeOut( "slow" );

});


/* ==============================================
 Calling Scroll Animations
 =============================================== */
'use strict';
AOS.init();


/* ==============================================
 Navigation Fixed on Scroll
 =============================================== */
$(window).scroll(function(){
    'use strict';
    var scroll = $(window).scrollTop();
    if (scroll >= 2)
    {
        $('nav').addClass('nav-fixed');
    }
    else {
        $('nav').removeClass('nav-fixed');
    }
});


/* ==============================================
 Navigation Click & Active on Reaching Section
 =============================================== */
'use strict';
var sections = $('section')
    , nav = $('nav')
    , nav_height = nav.outerHeight();

$(window).on('scroll', function () {
    'use strict';
    var cur_pos = $(this).scrollTop();

    sections.each(function() {
        var top = $(this).offset().top - nav_height - 90,
            bottom = top + $(this).outerHeight();

        if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');

            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
    });
});

var navlia = $('.nav li a');
$(navlia).on('click',function() {
    'use strict';
    var hreff = $(this).attr('href');
    $('html, body').animate({
        scrollTop: $(hreff).offset().top-60
    }, 800);
});

$('nav .logoo a').on('click',function() {
    'use strict';
    $(navlia).removeClass('active');
    $('.nav li:first-child a').addClass('active');
});





$(document).ready(function() {

    /* ==============================================
     Testimonial Carousel
     =============================================== */
    $("#testimonial").owlCarousel({
        autoPlay: 4000, //Set AutoPlay to 3 seconds
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,2],
        itemsMobile : [500,1]
    });

    /* ==============================================
     Accordian used in "What we can offer?" Section
     =============================================== */
    'use strict';
    var acordian = $(".acordian");
    $(acordian).on('click',function(){
        'use strict';
        if($(this).hasClass('active')){
            $(this).toggleClass('active')
        }
        else{
            $(acordian).removeClass('active');
            $(this).addClass('active');
        }
    });


    /* ==============================================
     Calling Nice Scroll
     =============================================== */
    'use strict';
    $("html").niceScroll();

});


/* ==============================================
 Scroll Back To Top
 =============================================== */

'use strict';
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    'use strict';
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
function topFunction() {
    $('body,html').animate({
        scrollTop: 0
    }, 1500);
}


