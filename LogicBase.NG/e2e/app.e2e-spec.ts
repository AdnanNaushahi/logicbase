import { LogicBase.NGPage } from './app.po';

describe('logic-base.ng App', () => {
  let page: LogicBase.NGPage;

  beforeEach(() => {
    page = new LogicBase.NGPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
